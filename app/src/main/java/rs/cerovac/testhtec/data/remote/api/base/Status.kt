package rs.cerovac.testhtec.data.remote.api.base

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    EMPTY
}