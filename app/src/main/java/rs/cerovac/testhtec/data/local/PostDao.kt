package rs.cerovac.testhtec.data.local

import androidx.room.*
import rs.cerovac.testhtec.models.PostModel

@Dao
interface PostDao {

    @Query("SELECT * FROM posts")
    fun getAllPosts() : List<PostModel>

    @Query("SELECT * FROM posts  LIMIT :initialItem, :totalItems")
    fun getPosts( initialItem: Int, totalItems: Int): List<PostModel>

    @Query("SELECT * FROM posts WHERE id = :id")
    fun getPost(id: Int): PostModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(postModel: List<PostModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(postModel: PostModel)

    @Delete
    suspend fun delete(postModel: PostModel)

    @Query("DELETE FROM posts")
    suspend fun clear()

}