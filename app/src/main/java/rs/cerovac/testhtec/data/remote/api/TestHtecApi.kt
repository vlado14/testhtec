package rs.cerovac.testhtec.data.remote.api

import retrofit2.Response
import retrofit2.http.*
import rs.cerovac.testhtec.models.PostModel
import rs.cerovac.testhtec.models.UserModel

interface TestHtecApi {

    @GET("posts")
    suspend fun getPosts(
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): Response<List<PostModel>>

    @GET("users/{userId}")
    suspend fun getUser(
        @Path("userId") userId: String
    ): Response<UserModel>

}