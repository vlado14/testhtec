package rs.cerovac.testhtec.data.datasource.posts

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import rs.cerovac.testhtec.data.local.PostDao
import rs.cerovac.testhtec.domain.api.ApiClient
import rs.cerovac.testhtec.models.PostModel

class PostsListDataSourceFactory(private val apiClient: ApiClient, private val postDao: PostDao) :
    DataSource.Factory<Int, PostModel>() {

    val liveData: MutableLiveData<PostsListDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, PostModel> {
        val postsListDataSource = PostsListDataSource(apiClient, postDao)
        liveData.postValue(postsListDataSource)
        return postsListDataSource
    }
}