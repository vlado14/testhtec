package rs.cerovac.testhtec.data.datasource.posts

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import rs.cerovac.testhtec.data.local.PostDao
import rs.cerovac.testhtec.data.remote.api.base.Status
import rs.cerovac.testhtec.domain.api.ApiClient
import rs.cerovac.testhtec.models.PostModel
import java.util.concurrent.TimeUnit

class PostsListDataSource(
    private val apiClient: ApiClient,
    private val postDao: PostDao
) : PageKeyedDataSource<Int, PostModel>() {

    private val dataSourceJob = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Main + dataSourceJob)
    val loadStateLiveData: MutableLiveData<Status> = MutableLiveData()

    companion object {
        const val PAGE_SIZE = 15
        const val CACHE_INTERVAL = 5
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, PostModel>
    ) {
        val dbResponse = postDao.getAllPosts()

        scope.launch {
            loadStateLiveData.postValue(Status.LOADING)
            if (dbResponse.isNullOrEmpty().not() &&
                TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - dbResponse[dbResponse.size-1].updateTime) < CACHE_INTERVAL) {

                dbResponse.let {
                    callback.onResult(it, null, 2)
                    loadStateLiveData.postValue(Status.SUCCESS)
                }
            } else {
                val response = apiClient.getPosts(1, PAGE_SIZE)
                when (response.status) {
                    Status.ERROR -> loadStateLiveData.postValue(Status.ERROR)
                    Status.EMPTY -> loadStateLiveData.postValue(Status.EMPTY)
                    else -> {
                        response.data?.forEach { it.updateTime = System.currentTimeMillis() }
                        response.data?.let { postDao.insertAll(it) }
                        response.data?.let {
                            callback.onResult(it, null, 2)
                            loadStateLiveData.postValue(Status.SUCCESS)
                        }
                    }
                }
            }

        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PostModel>) {
        val dbResponse = postDao.getPosts(params.key, PAGE_SIZE)
        scope.launch {
            if (dbResponse.isNullOrEmpty() &&
                TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - dbResponse[dbResponse.size-1].updateTime) > CACHE_INTERVAL) {
                val response = apiClient.getPosts(params.key, PAGE_SIZE)
                response.data?.forEach { it.updateTime = System.currentTimeMillis() }
                response.data?.let { postDao.insertAll(it) }
                response.data?.let {
                    callback.onResult(it, params.key + 1)
                }
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PostModel>) {

    }
}