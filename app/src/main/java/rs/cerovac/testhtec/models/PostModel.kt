package rs.cerovac.testhtec.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.sql.Timestamp
import java.time.Instant
import java.util.*

@Entity(tableName = "posts")
@Parcelize
data class PostModel(
    @SerializedName("userId") var userId: Long,
    @PrimaryKey
    @SerializedName("id") var id: Long,
    @SerializedName("title") var title: String,
    @SerializedName("body") var body: String,
    var updateTime: Long
): Parcelable