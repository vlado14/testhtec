package rs.cerovac.testhtec.models

import com.google.gson.annotations.SerializedName

data class UserModel(
    @SerializedName("id") var id: Long,
    @SerializedName("name") var name: String,
    @SerializedName("username") var username: String,
    @SerializedName("email") var email: String
)