package rs.cerovac.testhtec.domain.api

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import rs.cerovac.testhtec.data.remote.api.TestHtecApi
import rs.cerovac.testhtec.data.remote.api.base.Resource
import rs.cerovac.testhtec.models.PostModel
import rs.cerovac.testhtec.models.UserModel

class ApiClientImpl(private val testHtecApi: TestHtecApi) : ApiClient {

    override suspend fun getPosts(page: Int, pageSize: Int): Resource<List<PostModel>> = withContext(Dispatchers.IO) {
        try {
            val response = testHtecApi.getPosts(page, pageSize)
            if (response.isSuccessful) {
                Resource.success(response.body())
            } else {
                Resource.error(response.message())
            }
        } catch (ex: Throwable) {
            Resource.error("${ex.message}")
        }
    }
    
    override suspend fun getUser(userId: String): Resource<UserModel> = withContext(Dispatchers.IO) {
        try {
            val response = testHtecApi.getUser(userId)
            if (response.isSuccessful) {
                Resource.success(response.body())
            } else {
                Resource.error(response.message())
            }
        } catch (ex: Throwable) {
            Resource.error("${ex.message}")
        }
    }

}