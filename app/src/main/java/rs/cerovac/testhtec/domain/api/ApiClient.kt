package rs.cerovac.testhtec.domain.api

import rs.cerovac.testhtec.data.remote.api.base.Resource
import rs.cerovac.testhtec.models.PostModel
import rs.cerovac.testhtec.models.UserModel


interface ApiClient {

    suspend fun getPosts(page: Int, pageSize: Int): Resource<List<PostModel>>

    suspend fun getUser(userId: String): Resource<UserModel>

}