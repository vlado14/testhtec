package rs.cerovac.testhtec.ui.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Transformations
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.posts_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import rs.cerovac.testhtec.R
import rs.cerovac.testhtec.data.remote.api.base.Status
import rs.cerovac.testhtec.databinding.PostsFragmentBinding
import rs.cerovac.testhtec.models.PostModel

class PostsFragment : Fragment(), PostsListAdapter.PostsListAdapterInteraction {

    private val viewModel: PostsViewModel by viewModel()
    private lateinit var rvPosts: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: PostsFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.posts_fragment, container, false)

        binding.vm = viewModel
        rvPosts = binding.root.findViewById(R.id.rvPosts)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapterAndObserve()
        pullToRefresh.setOnRefreshListener {
            viewModel.refreshPosts()
            pullToRefresh.isRefreshing = false
        }
    }

    private fun initAdapterAndObserve() {
        viewModel.postsLiveData.value?.dataSource?.invalidate()

        val postsListAdapter = PostsListAdapter(this)

        rvPosts.layoutManager = LinearLayoutManager(context)
        rvPosts.adapter = postsListAdapter

        Transformations.switchMap(viewModel.dataSource) { dataSource -> dataSource.loadStateLiveData }
            .observe(viewLifecycleOwner, {
                when (it) {
                    Status.LOADING -> {
                        viewModel.isWaiting.set(true)
                        viewModel.errorMessage.set(null)
                    }
                    Status.SUCCESS -> {
                        viewModel.isWaiting.set(false)
                        viewModel.errorMessage.set(null)
                    }
                    Status.EMPTY -> {
                        viewModel.isWaiting.set(false)
                        viewModel.errorMessage.set(getString(R.string.msg_posts_list_is_empty))
                    }
                    else -> {
                        viewModel.isWaiting.set(false)
                        viewModel.errorMessage.set(getString(R.string.msg_fetch_posts_list_error))
                    }
                }
            })

        viewModel.postsLiveData.observe(viewLifecycleOwner, {
            postsListAdapter.submitList(it)
        })
    }

    override fun onPostItemClick(postModel: PostModel) {
        findNavController().navigate(
            PostsFragmentDirections.actionNavigationPostsToNavigationUserDetails(postModel)
        )
    }
}