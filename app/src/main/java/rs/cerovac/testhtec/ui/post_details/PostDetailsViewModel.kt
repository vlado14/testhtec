package rs.cerovac.testhtec.ui.post_details

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import rs.cerovac.testhtec.data.local.PostDao
import rs.cerovac.testhtec.data.remote.api.base.Resource
import rs.cerovac.testhtec.data.remote.api.base.Status
import rs.cerovac.testhtec.domain.api.ApiClient
import rs.cerovac.testhtec.models.PostModel
import rs.cerovac.testhtec.models.UserModel
import rs.cerovac.testhtec.utils.SingleLiveEvent

class PostDetailsViewModel(private val apiClient: ApiClient, private val postDao: PostDao) : ViewModel() {

    val userModel: ObservableField<UserModel> = ObservableField()

    private val _result: SingleLiveEvent<Resource<Unit>> = SingleLiveEvent()
    val result: LiveData<Resource<Unit>>
        get() = _result

    private val _onDeletePostClicked = SingleLiveEvent<Unit>()
    val onDeletePostClicked: LiveData<Unit>
        get() = _onDeletePostClicked

    fun deletePostButtonClicked() {
        _onDeletePostClicked.call()
    }

    fun getUserInfoById(userId: String) {
        viewModelScope.launch {
            val result = apiClient.getUser(userId)
            if (result.status == Status.SUCCESS) {
                userModel.set(result.data)
            } else {
                userModel.set(null)
            }
        }
    }

    fun deletePost(postModel: PostModel){
        _result.value = Resource.loading()
        viewModelScope.launch {
            postDao.delete(postModel)
            _result.value = Resource.success()
        }
    }
}