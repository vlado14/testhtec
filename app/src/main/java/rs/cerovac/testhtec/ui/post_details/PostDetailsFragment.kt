package rs.cerovac.testhtec.ui.post_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.koin.androidx.viewmodel.ext.android.viewModel
import rs.cerovac.testhtec.R
import rs.cerovac.testhtec.data.remote.api.base.Status
import rs.cerovac.testhtec.databinding.PostDetailsFragmentBinding
import rs.cerovac.testhtec.models.PostModel
import rs.cerovac.testhtec.utils.observeNotNull
import rs.cerovac.testhtec.utils.observeNullable

class PostDetailsFragment : Fragment() {

    private val viewModel: PostDetailsViewModel by viewModel()
    private lateinit var postModel: PostModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: PostDetailsFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.post_details_fragment, container, false)
        binding.vm = viewModel
        arguments?.let {
            postModel = PostDetailsFragmentArgs.fromBundle(it).postModel
            binding.postModel = postModel
            viewModel.getUserInfoById(postModel.userId.toString())
        }

        setupObservers()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.result.observeNotNull(viewLifecycleOwner) { state ->
            when (state.status) {
                Status.SUCCESS -> {
                    findNavController().popBackStack()
                }
                Status.ERROR -> {
                }
                Status.EMPTY -> {
                }
                Status.LOADING -> {
                }
            }
        }
    }

    private fun setupObservers() = with(viewModel) {
        onDeletePostClicked.observeNullable(viewLifecycleOwner) {
            viewModel.deletePost(postModel)
        }
    }
}
