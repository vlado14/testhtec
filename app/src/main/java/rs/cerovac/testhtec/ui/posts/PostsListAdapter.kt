package rs.cerovac.testhtec.ui.posts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import rs.cerovac.testhtec.R
import rs.cerovac.testhtec.models.PostModel

class PostsListAdapter(private val listener: PostsListAdapterInteraction) :
    PagedListAdapter<PostModel, PostsListAdapter.UsersListViewHolder>(
        usersDiffCallback
    ) {

    private lateinit var context: Context

    interface PostsListAdapterInteraction {
        fun onPostItemClick(postModel: PostModel)
    }

    inner class UsersListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val postItem: ConstraintLayout = itemView.findViewById(R.id.postItem)
        val title: TextView = itemView.findViewById(R.id.tvPostTitle)
        val body: TextView = itemView.findViewById(R.id.tvPostBody)
    }

    override fun onBindViewHolder(holder: UsersListViewHolder, position: Int) {
        val postModel = getItem(position)
        postModel?.let {

            holder.title.text = it.title
            holder.body.text = it.body

            holder.postItem.setOnClickListener {
                listener.onPostItemClick(postModel)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersListViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.li_post, parent, false)
        return UsersListViewHolder(view)
    }

    companion object {
        val usersDiffCallback = object : DiffUtil.ItemCallback<PostModel>() {
            override fun areItemsTheSame(
                oldItem: PostModel,
                newItem: PostModel
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: PostModel,
                newItem: PostModel
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}