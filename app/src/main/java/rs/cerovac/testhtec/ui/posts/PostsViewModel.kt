package rs.cerovac.testhtec.ui.posts

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.launch
import rs.cerovac.testhtec.data.datasource.posts.PostsListDataSource
import rs.cerovac.testhtec.data.datasource.posts.PostsListDataSourceFactory
import rs.cerovac.testhtec.data.local.PostDao
import rs.cerovac.testhtec.models.PostModel
import java.util.concurrent.Executors

class PostsViewModel(
    private val postsListDataSourceFactory: PostsListDataSourceFactory,
    private val postDao: PostDao
) : ViewModel() {

    lateinit var postsLiveData: LiveData<PagedList<PostModel>>
    var dataSource: MutableLiveData<PostsListDataSource>
    val isWaiting: ObservableField<Boolean> = ObservableField()
    val errorMessage: ObservableField<String> = ObservableField()

    init {
        isWaiting.set(true)
        errorMessage.set(null)
        dataSource = postsListDataSourceFactory.liveData
        initPostsListFactory()
    }

    private fun initPostsListFactory() {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(PostsListDataSource.PAGE_SIZE)
            .setPageSize(PostsListDataSource.PAGE_SIZE)
            .setPrefetchDistance(3)
            .build()

        val executor = Executors.newFixedThreadPool(5)

        postsLiveData = LivePagedListBuilder(postsListDataSourceFactory, config)
            .setFetchExecutor(executor)
            .build()
    }

    fun refreshPosts() {
        viewModelScope.launch {
            postDao.clear()
            postsLiveData.value?.dataSource?.invalidate()
        }
    }

}