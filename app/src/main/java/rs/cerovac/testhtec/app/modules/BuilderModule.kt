package rs.cerovac.testhtec.app.modules


val allModules = listOf(
        githubApiModule,
        githubApiClientModule,
        localDataSourceModule,
        postsListDataSourceFactory,
        postsViewModel,
        userDetailsViewModel
)