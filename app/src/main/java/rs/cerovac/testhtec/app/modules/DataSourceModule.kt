package rs.cerovac.testhtec.app.modules


import org.koin.dsl.module
import rs.cerovac.testhtec.data.datasource.posts.PostsListDataSourceFactory

val postsListDataSourceFactory = module {
    single { PostsListDataSourceFactory(get(), get()) }
}