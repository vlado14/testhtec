package rs.cerovac.testhtec.app.modules

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import rs.cerovac.testhtec.ui.posts.PostsViewModel
import rs.cerovac.testhtec.ui.post_details.PostDetailsViewModel

val postsViewModel = module {
    viewModel { PostsViewModel(get(), get()) }
}

val userDetailsViewModel = module {
    viewModel { PostDetailsViewModel(get(), get()) }
}


