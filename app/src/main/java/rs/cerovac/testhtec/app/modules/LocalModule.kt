package rs.cerovac.testhtec.app.modules

import android.content.Context
import org.koin.dsl.module
import rs.cerovac.testhtec.data.local.AppDatabase
import rs.cerovac.testhtec.data.local.PostDao

val localDataSourceModule = module {
    single { provideAffinityDatabase(get()) }
    single { providePostDao(get()) }
}

fun provideAffinityDatabase(context: Context): AppDatabase {
    return AppDatabase.getDatabase(context)
}

fun providePostDao(database: AppDatabase): PostDao {
    return database.postDao()
}