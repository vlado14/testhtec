package rs.cerovac.testhtec.ui.post_details

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavArgs
import java.io.Serializable
import java.lang.IllegalArgumentException
import java.lang.UnsupportedOperationException
import kotlin.Suppress
import kotlin.jvm.JvmStatic
import rs.cerovac.testhtec.models.PostModel

data class PostDetailsFragmentArgs(val postModel: PostModel) : NavArgs {
    @Suppress("CAST_NEVER_SUCCEEDS")
    fun toBundle(): Bundle {
        val result = Bundle()
        if (Parcelable::class.java.isAssignableFrom(PostModel::class.java)) {
            result.putParcelable("postModel", this.postModel as Parcelable)
        } else if (Serializable::class.java.isAssignableFrom(PostModel::class.java)) {
            result.putSerializable("postModel", this.postModel as Serializable)
        } else {
            throw UnsupportedOperationException(PostModel::class.java.name +
                    " must implement Parcelable or Serializable or must be an Enum.")
        }
        return result
    }

    companion object {
        @JvmStatic
        fun fromBundle(bundle: Bundle): PostDetailsFragmentArgs {
            bundle.setClassLoader(PostDetailsFragmentArgs::class.java.classLoader)
            val __postModel : PostModel?
            if (bundle.containsKey("postModel")) {
                if (Parcelable::class.java.isAssignableFrom(PostModel::class.java) ||
                        Serializable::class.java.isAssignableFrom(PostModel::class.java)) {
                    __postModel = bundle.get("postModel") as PostModel?
                } else {
                    throw UnsupportedOperationException(PostModel::class.java.name +
                            " must implement Parcelable or Serializable or must be an Enum.")
                }
                if (__postModel == null) {
                    throw IllegalArgumentException("Argument \"postModel\" is marked as non-null but was passed a null value.")
                }
            } else {
                throw IllegalArgumentException("Required argument \"postModel\" is missing and does not have an android:defaultValue")
            }
            return PostDetailsFragmentArgs(__postModel)
        }
    }
}
