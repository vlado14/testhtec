package rs.cerovac.testhtec.ui.posts

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavDirections
import java.io.Serializable
import java.lang.UnsupportedOperationException
import kotlin.Int
import kotlin.Suppress
import rs.cerovac.testhtec.R
import rs.cerovac.testhtec.models.PostModel

class PostsFragmentDirections private constructor() {
    private data class ActionNavigationPostsToNavigationUserDetails(val postModel: PostModel) :
            NavDirections {
        override fun getActionId(): Int = R.id.action_navigation_posts_to_navigation_user_details

        @Suppress("CAST_NEVER_SUCCEEDS")
        override fun getArguments(): Bundle {
            val result = Bundle()
            if (Parcelable::class.java.isAssignableFrom(PostModel::class.java)) {
                result.putParcelable("postModel", this.postModel as Parcelable)
            } else if (Serializable::class.java.isAssignableFrom(PostModel::class.java)) {
                result.putSerializable("postModel", this.postModel as Serializable)
            } else {
                throw UnsupportedOperationException(PostModel::class.java.name +
                        " must implement Parcelable or Serializable or must be an Enum.")
            }
            return result
        }
    }

    companion object {
        fun actionNavigationPostsToNavigationUserDetails(postModel: PostModel): NavDirections =
                ActionNavigationPostsToNavigationUserDetails(postModel)
    }
}
