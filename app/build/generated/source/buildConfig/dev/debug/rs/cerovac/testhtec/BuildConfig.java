/**
 * Automatically generated file. DO NOT MODIFY
 */
package rs.cerovac.testhtec;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "rs.cerovac.testhtec";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "dev";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Field from product flavor: dev
  public static final String API_BASE_URL = "https://jsonplaceholder.typicode.com/";
}
