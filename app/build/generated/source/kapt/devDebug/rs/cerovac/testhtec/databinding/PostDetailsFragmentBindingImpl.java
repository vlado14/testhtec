package rs.cerovac.testhtec.databinding;
import rs.cerovac.testhtec.R;
import rs.cerovac.testhtec.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class PostDetailsFragmentBindingImpl extends PostDetailsFragmentBinding implements rs.cerovac.testhtec.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public PostDetailsFragmentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private PostDetailsFragmentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.Button) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[1]
            );
        this.btnDeletePost.setTag(null);
        this.main.setTag(null);
        this.tvAuthorEmail.setTag(null);
        this.tvPostAuthor.setTag(null);
        this.tvPostBody.setTag(null);
        this.tvPostTitle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback1 = new rs.cerovac.testhtec.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.vm == variableId) {
            setVm((rs.cerovac.testhtec.ui.post_details.PostDetailsViewModel) variable);
        }
        else if (BR.postModel == variableId) {
            setPostModel((rs.cerovac.testhtec.models.PostModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVm(@Nullable rs.cerovac.testhtec.ui.post_details.PostDetailsViewModel Vm) {
        this.mVm = Vm;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.vm);
        super.requestRebind();
    }
    public void setPostModel(@Nullable rs.cerovac.testhtec.models.PostModel PostModel) {
        this.mPostModel = PostModel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.postModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVmUserModel((androidx.databinding.ObservableField<rs.cerovac.testhtec.models.UserModel>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVmUserModel(androidx.databinding.ObservableField<rs.cerovac.testhtec.models.UserModel> VmUserModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<rs.cerovac.testhtec.models.UserModel> vmUserModel = null;
        rs.cerovac.testhtec.models.UserModel vmUserModelGet = null;
        java.lang.String vmUserModelName = null;
        rs.cerovac.testhtec.ui.post_details.PostDetailsViewModel vm = mVm;
        java.lang.String postModelBody = null;
        java.lang.String postModelTitle = null;
        java.lang.String vmUserModelEmail = null;
        rs.cerovac.testhtec.models.PostModel postModel = mPostModel;

        if ((dirtyFlags & 0xbL) != 0) {



                if (vm != null) {
                    // read vm.userModel
                    vmUserModel = vm.getUserModel();
                }
                updateRegistration(0, vmUserModel);


                if (vmUserModel != null) {
                    // read vm.userModel.get()
                    vmUserModelGet = vmUserModel.get();
                }


                if (vmUserModelGet != null) {
                    // read vm.userModel.get().name
                    vmUserModelName = vmUserModelGet.getName();
                    // read vm.userModel.get().email
                    vmUserModelEmail = vmUserModelGet.getEmail();
                }
        }
        if ((dirtyFlags & 0xcL) != 0) {



                if (postModel != null) {
                    // read postModel.body
                    postModelBody = postModel.getBody();
                    // read postModel.title
                    postModelTitle = postModel.getTitle();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.btnDeletePost.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvAuthorEmail, vmUserModelEmail);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPostAuthor, vmUserModelName);
        }
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPostBody, postModelBody);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPostTitle, postModelTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // vm != null
        boolean vmJavaLangObjectNull = false;
        // vm
        rs.cerovac.testhtec.ui.post_details.PostDetailsViewModel vm = mVm;



        vmJavaLangObjectNull = (vm) != (null);
        if (vmJavaLangObjectNull) {


            vm.deletePostButtonClicked();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): vm.userModel
        flag 1 (0x2L): vm
        flag 2 (0x3L): postModel
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}