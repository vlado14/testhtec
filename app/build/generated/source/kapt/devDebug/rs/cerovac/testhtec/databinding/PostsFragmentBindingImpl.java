package rs.cerovac.testhtec.databinding;
import rs.cerovac.testhtec.R;
import rs.cerovac.testhtec.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class PostsFragmentBindingImpl extends PostsFragmentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.pullToRefresh, 4);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final android.widget.ProgressBar mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public PostsFragmentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private PostsFragmentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (androidx.swiperefreshlayout.widget.SwipeRefreshLayout) bindings[4]
            , (androidx.recyclerview.widget.RecyclerView) bindings[1]
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.ProgressBar) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.rvPosts.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.vm == variableId) {
            setVm((rs.cerovac.testhtec.ui.posts.PostsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVm(@Nullable rs.cerovac.testhtec.ui.posts.PostsViewModel Vm) {
        this.mVm = Vm;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.vm);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVmErrorMessage((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeVmIsWaiting((androidx.databinding.ObservableField<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVmErrorMessage(androidx.databinding.ObservableField<java.lang.String> VmErrorMessage, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVmIsWaiting(androidx.databinding.ObservableField<java.lang.Boolean> VmIsWaiting, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Boolean vmIsWaitingGet = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsWaiting = false;
        int androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingViewVISIBLEViewGONE = 0;
        boolean vmErrorMessageJavaLangObjectNull = false;
        int androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingBooleanTrueVmErrorMessageJavaLangObjectNullViewGONEViewVISIBLE = 0;
        androidx.databinding.ObservableField<java.lang.String> vmErrorMessage = null;
        androidx.databinding.ObservableField<java.lang.Boolean> vmIsWaiting = null;
        boolean VmErrorMessageJavaLangObjectNull1 = false;
        rs.cerovac.testhtec.ui.posts.PostsViewModel vm = mVm;
        boolean androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingBooleanTrueVmErrorMessageJavaLangObjectNull = false;
        java.lang.String vmErrorMessageGet = null;
        int vmErrorMessageJavaLangObjectNullViewGONEViewVISIBLE = 0;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (vm != null) {
                        // read vm.errorMessage
                        vmErrorMessage = vm.getErrorMessage();
                    }
                    updateRegistration(0, vmErrorMessage);


                    if (vmErrorMessage != null) {
                        // read vm.errorMessage.get()
                        vmErrorMessageGet = vmErrorMessage.get();
                    }


                    // read vm.errorMessage.get() == null
                    VmErrorMessageJavaLangObjectNull1 = (vmErrorMessageGet) == (null);
                if((dirtyFlags & 0xdL) != 0) {
                    if(VmErrorMessageJavaLangObjectNull1) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read vm.errorMessage.get() == null ? View.GONE : View.VISIBLE
                    vmErrorMessageJavaLangObjectNullViewGONEViewVISIBLE = ((VmErrorMessageJavaLangObjectNull1) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }

                if (vm != null) {
                    // read vm.isWaiting
                    vmIsWaiting = vm.isWaiting();
                }
                updateRegistration(1, vmIsWaiting);


                if (vmIsWaiting != null) {
                    // read vm.isWaiting.get()
                    vmIsWaitingGet = vmIsWaiting.get();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get())
                androidxDatabindingViewDataBindingSafeUnboxVmIsWaiting = androidx.databinding.ViewDataBinding.safeUnbox(vmIsWaitingGet);
            if((dirtyFlags & 0xeL) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxVmIsWaiting) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
            if((dirtyFlags & 0xfL) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxVmIsWaiting) {
                        dirtyFlags |= 0x200L;
                }
                else {
                        dirtyFlags |= 0x100L;
                }
            }

            if ((dirtyFlags & 0xeL) != 0) {

                    // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? View.VISIBLE : View.GONE
                    androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVmIsWaiting) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished

        if ((dirtyFlags & 0x100L) != 0) {

                if (vm != null) {
                    // read vm.errorMessage
                    vmErrorMessage = vm.getErrorMessage();
                }
                updateRegistration(0, vmErrorMessage);


                if (vmErrorMessage != null) {
                    // read vm.errorMessage.get()
                    vmErrorMessageGet = vmErrorMessage.get();
                }


                // read vm.errorMessage.get() != null
                vmErrorMessageJavaLangObjectNull = (vmErrorMessageGet) != (null);
        }

        if ((dirtyFlags & 0xfL) != 0) {

                // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? true : vm.errorMessage.get() != null
                androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingBooleanTrueVmErrorMessageJavaLangObjectNull = ((androidxDatabindingViewDataBindingSafeUnboxVmIsWaiting) ? (true) : (vmErrorMessageJavaLangObjectNull));
            if((dirtyFlags & 0xfL) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingBooleanTrueVmErrorMessageJavaLangObjectNull) {
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x40L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? true : vm.errorMessage.get() != null ? View.GONE : View.VISIBLE
                androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingBooleanTrueVmErrorMessageJavaLangObjectNullViewGONEViewVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingBooleanTrueVmErrorMessageJavaLangObjectNull) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            this.mboundView2.setVisibility(androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, vmErrorMessageGet);
            this.mboundView3.setVisibility(vmErrorMessageJavaLangObjectNullViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0xfL) != 0) {
            // api target 1

            this.rvPosts.setVisibility(androidxDatabindingViewDataBindingSafeUnboxVmIsWaitingBooleanTrueVmErrorMessageJavaLangObjectNullViewGONEViewVISIBLE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): vm.errorMessage
        flag 1 (0x2L): vm.isWaiting
        flag 2 (0x3L): vm
        flag 3 (0x4L): null
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? View.VISIBLE : View.GONE
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? View.VISIBLE : View.GONE
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? true : vm.errorMessage.get() != null ? View.GONE : View.VISIBLE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? true : vm.errorMessage.get() != null ? View.GONE : View.VISIBLE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? true : vm.errorMessage.get() != null
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(vm.isWaiting.get()) ? true : vm.errorMessage.get() != null
        flag 10 (0xbL): vm.errorMessage.get() == null ? View.GONE : View.VISIBLE
        flag 11 (0xcL): vm.errorMessage.get() == null ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}