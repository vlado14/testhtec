package rs.cerovac.testhtec.data.datasource.posts;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u001c2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u001cB\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ*\u0010\u0012\u001a\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u00152\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0017H\u0016J*\u0010\u0018\u001a\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u00152\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0017H\u0016J*\u0010\u0019\u001a\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u001a2\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u001bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lrs/cerovac/testhtec/data/datasource/posts/PostsListDataSource;", "Landroidx/paging/PageKeyedDataSource;", "", "Lrs/cerovac/testhtec/models/PostModel;", "apiClient", "Lrs/cerovac/testhtec/domain/api/ApiClient;", "postDao", "Lrs/cerovac/testhtec/data/local/PostDao;", "(Lrs/cerovac/testhtec/domain/api/ApiClient;Lrs/cerovac/testhtec/data/local/PostDao;)V", "dataSourceJob", "Lkotlinx/coroutines/CompletableJob;", "loadStateLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lrs/cerovac/testhtec/data/remote/api/base/Status;", "getLoadStateLiveData", "()Landroidx/lifecycle/MutableLiveData;", "scope", "Lkotlinx/coroutines/CoroutineScope;", "loadAfter", "", "params", "Landroidx/paging/PageKeyedDataSource$LoadParams;", "callback", "Landroidx/paging/PageKeyedDataSource$LoadCallback;", "loadBefore", "loadInitial", "Landroidx/paging/PageKeyedDataSource$LoadInitialParams;", "Landroidx/paging/PageKeyedDataSource$LoadInitialCallback;", "Companion", "app_devDebug"})
public final class PostsListDataSource extends androidx.paging.PageKeyedDataSource<java.lang.Integer, rs.cerovac.testhtec.models.PostModel> {
    private final kotlinx.coroutines.CompletableJob dataSourceJob = null;
    private final kotlinx.coroutines.CoroutineScope scope = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<rs.cerovac.testhtec.data.remote.api.base.Status> loadStateLiveData = null;
    private final rs.cerovac.testhtec.domain.api.ApiClient apiClient = null;
    private final rs.cerovac.testhtec.data.local.PostDao postDao = null;
    public static final int PAGE_SIZE = 15;
    public static final int CACHE_INTERVAL = 5;
    @org.jetbrains.annotations.NotNull()
    public static final rs.cerovac.testhtec.data.datasource.posts.PostsListDataSource.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<rs.cerovac.testhtec.data.remote.api.base.Status> getLoadStateLiveData() {
        return null;
    }
    
    @java.lang.Override()
    public void loadInitial(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadInitialParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadInitialCallback<java.lang.Integer, rs.cerovac.testhtec.models.PostModel> callback) {
    }
    
    @java.lang.Override()
    public void loadAfter(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadCallback<java.lang.Integer, rs.cerovac.testhtec.models.PostModel> callback) {
    }
    
    @java.lang.Override()
    public void loadBefore(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadCallback<java.lang.Integer, rs.cerovac.testhtec.models.PostModel> callback) {
    }
    
    public PostsListDataSource(@org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.domain.api.ApiClient apiClient, @org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.data.local.PostDao postDao) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lrs/cerovac/testhtec/data/datasource/posts/PostsListDataSource$Companion;", "", "()V", "CACHE_INTERVAL", "", "PAGE_SIZE", "app_devDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}