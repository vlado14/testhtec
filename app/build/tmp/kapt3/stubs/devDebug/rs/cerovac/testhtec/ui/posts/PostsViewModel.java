package rs.cerovac.testhtec.ui.posts;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u001d\u001a\u00020\u001eH\u0002J\u0006\u0010\u001f\u001a\u00020\u001eR \u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\u000f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R&\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001c\u00a8\u0006 "}, d2 = {"Lrs/cerovac/testhtec/ui/posts/PostsViewModel;", "Landroidx/lifecycle/ViewModel;", "postsListDataSourceFactory", "Lrs/cerovac/testhtec/data/datasource/posts/PostsListDataSourceFactory;", "postDao", "Lrs/cerovac/testhtec/data/local/PostDao;", "(Lrs/cerovac/testhtec/data/datasource/posts/PostsListDataSourceFactory;Lrs/cerovac/testhtec/data/local/PostDao;)V", "dataSource", "Landroidx/lifecycle/MutableLiveData;", "Lrs/cerovac/testhtec/data/datasource/posts/PostsListDataSource;", "getDataSource", "()Landroidx/lifecycle/MutableLiveData;", "setDataSource", "(Landroidx/lifecycle/MutableLiveData;)V", "errorMessage", "Landroidx/databinding/ObservableField;", "", "getErrorMessage", "()Landroidx/databinding/ObservableField;", "isWaiting", "", "postsLiveData", "Landroidx/lifecycle/LiveData;", "Landroidx/paging/PagedList;", "Lrs/cerovac/testhtec/models/PostModel;", "getPostsLiveData", "()Landroidx/lifecycle/LiveData;", "setPostsLiveData", "(Landroidx/lifecycle/LiveData;)V", "initPostsListFactory", "", "refreshPosts", "app_devDebug"})
public final class PostsViewModel extends androidx.lifecycle.ViewModel {
    public androidx.lifecycle.LiveData<androidx.paging.PagedList<rs.cerovac.testhtec.models.PostModel>> postsLiveData;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<rs.cerovac.testhtec.data.datasource.posts.PostsListDataSource> dataSource;
    @org.jetbrains.annotations.NotNull()
    private final androidx.databinding.ObservableField<java.lang.Boolean> isWaiting = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.databinding.ObservableField<java.lang.String> errorMessage = null;
    private final rs.cerovac.testhtec.data.datasource.posts.PostsListDataSourceFactory postsListDataSourceFactory = null;
    private final rs.cerovac.testhtec.data.local.PostDao postDao = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<androidx.paging.PagedList<rs.cerovac.testhtec.models.PostModel>> getPostsLiveData() {
        return null;
    }
    
    public final void setPostsLiveData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<androidx.paging.PagedList<rs.cerovac.testhtec.models.PostModel>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<rs.cerovac.testhtec.data.datasource.posts.PostsListDataSource> getDataSource() {
        return null;
    }
    
    public final void setDataSource(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<rs.cerovac.testhtec.data.datasource.posts.PostsListDataSource> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableField<java.lang.Boolean> isWaiting() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableField<java.lang.String> getErrorMessage() {
        return null;
    }
    
    private final void initPostsListFactory() {
    }
    
    public final void refreshPosts() {
    }
    
    public PostsViewModel(@org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.data.datasource.posts.PostsListDataSourceFactory postsListDataSourceFactory, @org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.data.local.PostDao postDao) {
        super();
    }
}