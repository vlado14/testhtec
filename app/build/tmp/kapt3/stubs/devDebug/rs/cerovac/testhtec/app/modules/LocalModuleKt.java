package rs.cerovac.testhtec.app.modules;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u000e\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0005\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0003\u00a8\u0006\u000b"}, d2 = {"localDataSourceModule", "Lorg/koin/core/module/Module;", "getLocalDataSourceModule", "()Lorg/koin/core/module/Module;", "provideAffinityDatabase", "Lrs/cerovac/testhtec/data/local/AppDatabase;", "context", "Landroid/content/Context;", "providePostDao", "Lrs/cerovac/testhtec/data/local/PostDao;", "database", "app_devDebug"})
public final class LocalModuleKt {
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module localDataSourceModule = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final org.koin.core.module.Module getLocalDataSourceModule() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final rs.cerovac.testhtec.data.local.AppDatabase provideAffinityDatabase(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final rs.cerovac.testhtec.data.local.PostDao providePostDao(@org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.data.local.AppDatabase database) {
        return null;
    }
}