package rs.cerovac.testhtec.ui.posts;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00122\u0012\u0012\u0004\u0012\u00020\u0002\u0012\b\u0012\u00060\u0003R\u00020\u00000\u0001:\u0003\u0012\u0013\u0014B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u00060\u0003R\u00020\u00002\u0006\u0010\f\u001a\u00020\rH\u0016J\u001c\u0010\u000e\u001a\u00060\u0003R\u00020\u00002\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\rH\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lrs/cerovac/testhtec/ui/posts/PostsListAdapter;", "Landroidx/paging/PagedListAdapter;", "Lrs/cerovac/testhtec/models/PostModel;", "Lrs/cerovac/testhtec/ui/posts/PostsListAdapter$UsersListViewHolder;", "listener", "Lrs/cerovac/testhtec/ui/posts/PostsListAdapter$PostsListAdapterInteraction;", "(Lrs/cerovac/testhtec/ui/posts/PostsListAdapter$PostsListAdapterInteraction;)V", "context", "Landroid/content/Context;", "onBindViewHolder", "", "holder", "position", "", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Companion", "PostsListAdapterInteraction", "UsersListViewHolder", "app_devDebug"})
public final class PostsListAdapter extends androidx.paging.PagedListAdapter<rs.cerovac.testhtec.models.PostModel, rs.cerovac.testhtec.ui.posts.PostsListAdapter.UsersListViewHolder> {
    private android.content.Context context;
    private final rs.cerovac.testhtec.ui.posts.PostsListAdapter.PostsListAdapterInteraction listener = null;
    @org.jetbrains.annotations.NotNull()
    private static final androidx.recyclerview.widget.DiffUtil.ItemCallback<rs.cerovac.testhtec.models.PostModel> usersDiffCallback = null;
    @org.jetbrains.annotations.NotNull()
    public static final rs.cerovac.testhtec.ui.posts.PostsListAdapter.Companion Companion = null;
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.ui.posts.PostsListAdapter.UsersListViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public rs.cerovac.testhtec.ui.posts.PostsListAdapter.UsersListViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    public PostsListAdapter(@org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.ui.posts.PostsListAdapter.PostsListAdapterInteraction listener) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lrs/cerovac/testhtec/ui/posts/PostsListAdapter$PostsListAdapterInteraction;", "", "onPostItemClick", "", "postModel", "Lrs/cerovac/testhtec/models/PostModel;", "app_devDebug"})
    public static abstract interface PostsListAdapterInteraction {
        
        public abstract void onPostItemClick(@org.jetbrains.annotations.NotNull()
        rs.cerovac.testhtec.models.PostModel postModel);
    }
    
    @kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\b\u00a8\u0006\u000f"}, d2 = {"Lrs/cerovac/testhtec/ui/posts/PostsListAdapter$UsersListViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Lrs/cerovac/testhtec/ui/posts/PostsListAdapter;Landroid/view/View;)V", "body", "Landroid/widget/TextView;", "getBody", "()Landroid/widget/TextView;", "postItem", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getPostItem", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "title", "getTitle", "app_devDebug"})
    public final class UsersListViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final androidx.constraintlayout.widget.ConstraintLayout postItem = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView title = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView body = null;
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.constraintlayout.widget.ConstraintLayout getPostItem() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTitle() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getBody() {
            return null;
        }
        
        public UsersListViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lrs/cerovac/testhtec/ui/posts/PostsListAdapter$Companion;", "", "()V", "usersDiffCallback", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lrs/cerovac/testhtec/models/PostModel;", "getUsersDiffCallback", "()Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "app_devDebug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.recyclerview.widget.DiffUtil.ItemCallback<rs.cerovac.testhtec.models.PostModel> getUsersDiffCallback() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}