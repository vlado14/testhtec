package rs.cerovac.testhtec.domain.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J-\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00062\u0006\u0010\u000f\u001a\u00020\u0010H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0012"}, d2 = {"Lrs/cerovac/testhtec/domain/api/ApiClientImpl;", "Lrs/cerovac/testhtec/domain/api/ApiClient;", "testHtecApi", "Lrs/cerovac/testhtec/data/remote/api/TestHtecApi;", "(Lrs/cerovac/testhtec/data/remote/api/TestHtecApi;)V", "getPosts", "Lrs/cerovac/testhtec/data/remote/api/base/Resource;", "", "Lrs/cerovac/testhtec/models/PostModel;", "page", "", "pageSize", "(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getUser", "Lrs/cerovac/testhtec/models/UserModel;", "userId", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_devDebug"})
public final class ApiClientImpl implements rs.cerovac.testhtec.domain.api.ApiClient {
    private final rs.cerovac.testhtec.data.remote.api.TestHtecApi testHtecApi = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getPosts(int page, int pageSize, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super rs.cerovac.testhtec.data.remote.api.base.Resource<? extends java.util.List<rs.cerovac.testhtec.models.PostModel>>> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getUser(@org.jetbrains.annotations.NotNull()
    java.lang.String userId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super rs.cerovac.testhtec.data.remote.api.base.Resource<rs.cerovac.testhtec.models.UserModel>> p1) {
        return null;
    }
    
    public ApiClientImpl(@org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.data.remote.api.TestHtecApi testHtecApi) {
        super();
    }
}