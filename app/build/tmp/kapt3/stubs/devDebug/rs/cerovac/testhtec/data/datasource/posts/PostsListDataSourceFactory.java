package rs.cerovac.testhtec.data.datasource.posts;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0014\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u000fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lrs/cerovac/testhtec/data/datasource/posts/PostsListDataSourceFactory;", "Landroidx/paging/DataSource$Factory;", "", "Lrs/cerovac/testhtec/models/PostModel;", "apiClient", "Lrs/cerovac/testhtec/domain/api/ApiClient;", "postDao", "Lrs/cerovac/testhtec/data/local/PostDao;", "(Lrs/cerovac/testhtec/domain/api/ApiClient;Lrs/cerovac/testhtec/data/local/PostDao;)V", "liveData", "Landroidx/lifecycle/MutableLiveData;", "Lrs/cerovac/testhtec/data/datasource/posts/PostsListDataSource;", "getLiveData", "()Landroidx/lifecycle/MutableLiveData;", "create", "Landroidx/paging/DataSource;", "app_devDebug"})
public final class PostsListDataSourceFactory extends androidx.paging.DataSource.Factory<java.lang.Integer, rs.cerovac.testhtec.models.PostModel> {
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<rs.cerovac.testhtec.data.datasource.posts.PostsListDataSource> liveData = null;
    private final rs.cerovac.testhtec.domain.api.ApiClient apiClient = null;
    private final rs.cerovac.testhtec.data.local.PostDao postDao = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<rs.cerovac.testhtec.data.datasource.posts.PostsListDataSource> getLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.paging.DataSource<java.lang.Integer, rs.cerovac.testhtec.models.PostModel> create() {
        return null;
    }
    
    public PostsListDataSourceFactory(@org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.domain.api.ApiClient apiClient, @org.jetbrains.annotations.NotNull()
    rs.cerovac.testhtec.data.local.PostDao postDao) {
        super();
    }
}